var room_name;
var cur_user;
var message = document.getElementById('message');
var send = document.getElementById('send');
var total_chat = [];
var first_count = 0;
var second_count = 0;
var chat_ref;

const messaging = firebase.messaging();
messaging.usePublicVapidKey('BNQYKLuc6M7JkPf2lOVxDbgP13EFH_nst6-_g1YKnfXkkYGmvcXPEcJsve67Ehr9hcj-s5a8jtR3vbgQMzkpPbs');


firebase.auth().onAuthStateChanged(function (user) {
    if (user) {

        cur_user = user.displayName;
        firebase.database().ref('member/' + cur_user).once('value').then(function (snapshot) {
            room_name = snapshot.val().roomnow;
        }).then(function () {
            chat_ref = 'chatroom/' + room_name + '/chat';
            document.getElementById('chatroom_name').innerHTML = "room: " + room_name;

            firebase.database().ref(chat_ref).once('value').then(function (snapshot) {
                for (var i in snapshot.val()) {
                    first_count++;
                    var name = document.createTextNode(snapshot.val()[i].name);
                    var name_div = document.createElement('div');
                    name_div.className = 'text-white';
                    name_div.appendChild(name);


                    var text = document.createTextNode(snapshot.val()[i].data);

                    var span = document.createElement('span');
                    span.appendChild(text);
                    span.className = "border border-dark rounded-pill align-self-end"

                    var text_div = document.createElement('div');
                    text_div.appendChild(span);
                    if (snapshot.val()[i].name == cur_user) {
                        text_div.className += "row justify-content-end";
                        name_div.className += " row justify-content-end";
                        span.className += ' bg-success';
                    }
                    else span.className += ' bg-white';

                    document.getElementById('chatroom').appendChild(name_div);
                    document.getElementById('chatroom').appendChild(text_div);
                    var br = document.createElement("br");
                    document.getElementById('chatroom').appendChild(br);
                }

                firebase.database().ref(chat_ref).on('child_added', function (new_snapshot) {
                    second_count++;
                    if (second_count > first_count) {

                        var name = document.createTextNode(new_snapshot.val().name);
                        var name_div = document.createElement('div');
                        name_div.className = 'text-white';
                        name_div.appendChild(name);

                        var text = document.createTextNode(new_snapshot.val().data);

                        var span = document.createElement('span');
                        span.appendChild(text);
                        span.className = "border border-dark rounded-pill align-self-end"

                        var text_div = document.createElement('div');
                        text_div.appendChild(span);
                        if (new_snapshot.val().name == cur_user) {
                            text_div.className += "row justify-content-end";
                            name_div.className += " row justify-content-end";
                            span.className += ' bg-success';
                        }
                        else span.className += ' bg-white';

                        document.getElementById('chatroom').appendChild(name_div);
                        document.getElementById('chatroom').appendChild(text_div);
                        var br = document.createElement("br");
                        document.getElementById('chatroom').appendChild(br);
                    }
                })
            })

        })

        document.getElementById('username').innerHTML = user.displayName;
        send.addEventListener('click', function () {
            if (message.value != '') {
                firebase.database().ref(chat_ref).push(
                    {
                        name: cur_user,
                        data: message.value
                    }
                )
                message.value = '';
            }
        })


    }
})

document.getElementById('invite').addEventListener('click', function () {

    var invite_name = document.getElementById("invite_name").value;
    var exist = false;
    if (invite_name == "") return;
    firebase.database().ref('member').once('value').then(function (snapshot) {
        for (var i in snapshot.val()) {
            if (invite_name == snapshot.val()[i].username) {
                exist = true;
            }
        }
    }).then(function () {
        if (exist == true) {
            firebase.database().ref('member/' + invite_name + '/chatroom').push(room_name);
            firebase.database().ref('chatroom/' + room_name + '/' + 'user').push(invite_name);
            document.getElementById('invite_name').value = "";
        }
        else {
            alert("Not found this user");
            document.getElementById('invite_name').value = "";
        }
    })

})

