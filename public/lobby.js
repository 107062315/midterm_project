var cur_user;
var first_count = 0;
var second_count = 0;
var message = document.getElementById('message');
var send = document.getElementById('send');

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        cur_user = user.displayName;
        firebase.database().ref('lobby/chat').once('value').then(function (snapshot) {
            for (var i in snapshot.val()) {
                first_count++;
                var name = document.createTextNode(snapshot.val()[i].name);
                var name_div = document.createElement('div');
                name_div.className = 'text-white';
                name_div.appendChild(name);

                var text = document.createTextNode(snapshot.val()[i].data);

                var span = document.createElement('span');
                span.appendChild(text);
                span.className = "border border-dark rounded-pill align-self-end"

                var text_div = document.createElement('div');
                text_div.appendChild(span);
                if (snapshot.val()[i].name == cur_user) {
                    text_div.className += "row justify-content-end";
                    name_div.className += " row justify-content-end";
                    span.className += ' bg-success';
                }
                else span.className += ' bg-white';

                document.getElementById('chatroom').appendChild(name_div);
                document.getElementById('chatroom').appendChild(text_div);
                var br = document.createElement("br");
                document.getElementById('chatroom').appendChild(br);
            }

            firebase.database().ref('lobby/chat').on('child_added',function(new_snapshot)
            {
                second_count++;
                if (second_count > first_count) {

                    var name = document.createTextNode(new_snapshot.val().name);
                    var name_div = document.createElement('div');
                    name_div.className = 'text-white';
                    name_div.appendChild(name);

                    var text = document.createTextNode(new_snapshot.val().data);

                    var span = document.createElement('span');
                    span.appendChild(text);
                    span.className = "border border-dark rounded-pill align-self-end"

                    var text_div = document.createElement('div');
                    text_div.appendChild(span);
                    if (new_snapshot.val().name == cur_user) {
                        text_div.className += "row justify-content-end";
                        name_div.className += " row justify-content-end";
                        span.className += ' bg-success';
                    }
                    else span.className += ' bg-white';

                    document.getElementById('chatroom').appendChild(name_div);
                    document.getElementById('chatroom').appendChild(text_div);
                    var br = document.createElement("br");
                    document.getElementById('chatroom').appendChild(br);
                }
            })

        })

        document.getElementById('username').innerHTML = user.displayName;
        send.addEventListener('click', function () {
            if (message.value != '') {
                firebase.database().ref('lobby/chat').push(
                    {
                        name: cur_user,
                        data: message.value
                    }
                )
                message.value = '';
            }
        })

    }
})

