const messaging = firebase.messaging();
messaging.usePublicVapidKey('BNQYKLuc6M7JkPf2lOVxDbgP13EFH_nst6-_g1YKnfXkkYGmvcXPEcJsve67Ehr9hcj-s5a8jtR3vbgQMzkpPbs');

function init() {
    var username = document.getElementById('username');
    var cur_user;
    var room_button_list = [];
    var count = 0;
    var roomname = document.getElementById('roomname');


    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            cur_user = user.displayName;
            username.innerHTML = cur_user;

            //推撥
            var first_count=0;
            var second_count=0;
            firebase.database().ref('lobby/chat').once('value').then(function(snapshot)
            {
                for(var i in snapshot.val())
                {
                    first_count++;
                }
                firebase.database().ref('lobby/chat').on('child_added',function(new_snapshot)
                {
                    second_count++;
                    if(second_count>first_count)
                    {
                            messaging.getToken().then(function (token) {
                            console.log(token);
                            var notification = {
                                'title': 'Lobby: '+new_snapshot.val().name,
                                'body': new_snapshot.val().data,
                                'data': {
                                    'url': 'https://www.domain.com.tw'
                                }
                            };
            
                            fetch('https://fcm.googleapis.com/fcm/send', {
                                'method': 'POST',
                                'headers': {
                                    'Authorization': 'key=AAAArQI76u4:APA91bGtxN8cNtmsGT6uVEBOZamWtjUUqs_kxkQnYWBOCcHQjw7Vm-d81cmEPVM4qJ4AN7yNyXM3P0JbMLFBpHbuynRCzsPMBLY58j6gh59mow43llq9WNr6xey1vHly3-8r6a-rMzjf',
                                    'Content-Type': 'application/json'
                                },
                                'body': JSON.stringify({
                                    'data': notification,
                                    'to': token,
                                })
                            }).then(function (response) {
                                console.log(response);
                            }).catch(function (error) {
                                alert(error);
                            });
            
                            messaging.onMessage(function (payload) {
                                var msgTitle = payload.data.title;
                                var url = payload.data.click_action;
                                var notification = new Notification(msgTitle, payload.data);
                            });
                        })
                    }
                })
                
            })


            //登出按鈕
            document.getElementById('btn_signout').addEventListener('click', function () {
                firebase.auth().signOut().then(
                    function () {
                        alert("You have signed out successfully");
                        window.location.href = "index.html";
                    },
                    function (error) {
                        const errorCode = error.code;
                        const errorMessage = error.message;
                        alert(errorMessage);
                    }
                )
            })

            //創建新的聊天室
            document.getElementById('btn_create').addEventListener('click', function () {

                var room_existed=false;
                firebase.database().ref('chatroom').once('value').then(function(snapshot)
                {
                    for(var i in snapshot.val())
                    {
                        if(snapshot.val()[i].roomname==roomname.value)
                        {
                            room_existed=true;
                        }
                    }
                }).then(function()
                {
                    if(room_existed==false)
                    {
                        if (roomname.value != "") {
                            room_button_list.push(roomname.value);
                            firebase.database().ref('member/' + cur_user + '/chatroom').push(roomname.value);
                            firebase.database().ref('chatroom/' + roomname.value).set(
                                {
                                    roomname: roomname.value,
                                    chat: ""
                                }
                            )
                            firebase.database().ref('chatroom/' + roomname.value + '/user').push(cur_user);
                            roomname.value = '';
                        }
                    }
                    else
                    {
                        alert('This room has existed');
                        roomname.value="";
                    }
                })

            })

            //載入所有聊天室並做成按鈕
            firebase.database().ref('member/' + cur_user + '/chatroom').once('value').then(
                function (snapshot) {
                    for (var i in snapshot.val()) {
                        var room_select = document.getElementById('room_select');
                        var new_option = new Option(snapshot.val()[i], snapshot.val()[i]);
                        room_select.options.add(new_option);
                    }
                    firebase.database().ref('member/' + cur_user + '/chatroom').on('child_added', function (new_snapshot) {
                        count++;
                        if (count > document.getElementById('room_select').length) {
                            var new_option = new Option(new_snapshot.val(), new_snapshot.val());
                            document.getElementById('room_select').options.add(new_option);
                        }
                    })
                }
            )
        }

        document.getElementById('select').addEventListener('click', function () {
            var cur_room = document.getElementById('room_select').value;
            firebase.database().ref('member/' + cur_user + '/' + 'roomnow').set(cur_room);
            window.location.href = 'chatroom.html';
        })

    });
}
window.onload = function () {
    init();
};
