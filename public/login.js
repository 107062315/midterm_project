function sign_in() {
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    firebase.auth().signInWithEmailAndPassword(email, password).then(
        function () {
            window.location.href = "usermain.html";
        },
        function (error) {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert(errorMessage);
        }
    )
}

function sign_in_google() {
    var user_existed = false;
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function (result) {
        var username = result.user.displayName;
        var profile = {
            email: result.user.email,
            username: username
        };
        firebase.database().ref('member').once('value').then(function (snapshot) {
            for (var i in snapshot.val()) {
                console.log(snapshot.val()[i]);
                if (snapshot.val()[i].username == username) user_existed = true;
            }
        }).then(function () {
            if(user_existed==false)firebase.database().ref('member/' + username).set(profile);
        }).then(function(){window.location.href='usermain.html';})

    })
}