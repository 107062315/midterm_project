var database = firebase.database();
var samename = false;

function sign_up() {
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var username = document.getElementById("username").value;

    //username不能空白
    if (username == "") {
        alert("username can not be empty");
        return;
    }

    var profile = {
        email: email,
        password: password,
        username: username
    };

    firebase.auth().createUserWithEmailAndPassword(email, password).then(
        function(){
            firebase.auth().signInWithEmailAndPassword(email, password);
        }
    ).then(function () {
        //註冊時設定username
        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                user.updateProfile(
                    {
                        displayName: username
                    }
                ).then(function()
                {
                    firebase.database().ref('member/' + username).set(profile);
                    window.location.href = "usermain.html";
                })
            }
        });

    })
}