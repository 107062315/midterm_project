# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : midterm-project
* Key functions (add/delete)
    1.使用者在登入之後可以輸入roomname來創建聊天室，而透過下方的選單可以選擇要進入哪間聊天室，進入聊天室後可以透過輸入別人的username邀請別人進來，被邀請的人的房間選單就會出現那間聊天室可以選擇，所以私人聊天室可以透過自己創建或是被別人邀請進入

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-2e878.web.app

# Components Description : 
打開網頁後的第一頁有Login和Sign up兩個按鈕，透過Sign up可以用email註冊新的member，註冊後會自動登入，點Login按鈕後可以讓已經有的成員透過email登入也可以透過google註冊或登入。登入後，畫面的左上角會顯示使用者自己取的username，再右邊是登出按鈕，最右邊是Lobby也就是大廳，使用者可以自己輸入roomname來創建私人聊天室，而下方的選單會顯示使用者目前有在哪些房間裡，select其中一間房間就可以進入該聊天室，進入聊天室後可以輸入其他使用者的username來將其他使用者加入聊天室，而被邀請加入聊天室的人他的房間選單就會出現該房間可以select。而chrome notification的部分，當使用者停留在登入後選房間的頁面時，如果有別人在大廳發言，就會跳出通知顯示誰說了什麼

# Other Functions Description : 



